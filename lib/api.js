var request = require('request');

var bignumJSON = require('json-bignum');

var httpgg = require('http-get');

var long = require('long');

var WebApi = function(apiKey) {
	require('events').EventEmitter.call(this);
	this.apiKey = apiKey;
	this.urlGetTournaments = 'http://api.steampowered.com/IDOTA2Match_570/GetLeagueListing/v1/?key={0}&language={1}';
	this.urlGetTeamInfo = 'http://api.steampowered.com/IDOTA2Match_570/GetTeamInfoByTeamID/v1?key={0}&teams_requested={1}&start_at_team_id={2}';
	this.urlGetUGCFile = 'http://api.steampowered.com/ISteamRemoteStorage/GetUGCFileDetails/v1/?key={0}&appid=570&ugcid={1}';
	this.urlGetSchema = 'http://api.steampowered.com/IEconItems_570/GetSchema/v0001/?key={0}&language={1}';
	this.urlGetMatchDetails = 'http://api.steampowered.com/IDOTA2Match_570/GetMatchDetails/V001/?key={0}&match_id={1}';
	this.urlGetMatchHistory = 'http://api.steampowered.com/IDOTA2Match_570/GetMatchHistory/V001/?key={0}&hero_id={1}&game_mode={2}&skill={3}&min_players={4}&account_id={5}&league_id={6}&start_at_match_id={7}&matches_requested={8}&tournament_games_only={9}';
	this.urlGetMatchHistoryBySequenceNum = 'http://api.steampowered.com/IDOTA2Match_570/GetMatchHistoryBySequenceNum/v001/?key={0}&start_at_match_seq_num={1}&matches_requested={2}';
	this.urlGetHeroes = 'http://api.steampowered.com/IEconDOTA2_570/GetHeroes/v0001/?key={0}&language={1}';
	this.urlGetPlayer = 'http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key={0}&steamids={1}'
};

require('util').inherits(WebApi, require('events').EventEmitter);

WebApi.prototype.getTournaments = function(lang, cb){
	var url = this.urlGetTournaments.format(this.apiKey, lang);
	request(url, function(e, response, body) {
		if(e) {
			cb(e);
		} else {
			if(response.statusCode == 200) {
				cb(null, bignumJSON.parse(body).result.leagues);
			} else {
				cb({error: true, errorStatusCode: response.statusCode});
			}
		}
	});
};

WebApi.prototype.getMatchDetails = function(matchId, cb) {
	var url = this.urlGetMatchDetails.format(this.apiKey, matchId);
	request(url, function(e, response, body) {
		if(e) {
			cb(e);
		} else {
			if(response.statusCode == 200) {
				var result = bignumJSON.parse(body).result;
				if(result.players.length > 0) {
					cb(null, result);
				} else {
					cb({error: true, errorMsg: 'Match not found.'});
				}
			} else {
				cb({error: true, errorStatusCode: response.statusCode});
			}
		}
	});
};

WebApi.prototype.getMatchHistory = function(hero_id, game_mode, skill, min_players, account_id, league_id, start_at_match_id, matches_requested, tournament_games_only, cb) {
	var url = this.urlGetMatchHistory.format(this.apiKey, hero_id, game_mode, skill, min_players, account_id, league_id, start_at_match_id, matches_requested, tournament_games_only);
	request(url, function(e, response, body) {
		if(e) {
			cb(e);
		} else {
			if(response.statusCode == 200) {
				var result = bignumJSON.parse(body).result;
				if(result.num_results > 0) {
					cb(null, result);
				} else {
					cb({error: true, errorMsg: 'No matches found.'});
				}
			} else {
				cb({error: true, errorStatusCode: response.statusCode});
			}
		}
	});
};

WebApi.prototype.getMatchHistoryBySequenceNum = function(start_at_match_seq_num, matches_requested, cb) {
	var url = this.urlGetMatchHistoryBySequenceNum.format(this.apiKey, start_at_match_seq_num, matches_requested);
	console.log(url);
	request(url, function(e, response, body) {
		if(e) {
			cb(e);
		} else {
			if(response.statusCode == 200) {
				var result = bignumJSON.parse(body).result;
				if(result.matches.length > 0) {
					cb(null, result);
				} else {
					cb({error: true, errorMsg: 'No matches found.'});
				}
			} else {
				cb({error: true, errorStatusCode: response.statusCode});
			}
		}
	});
};

WebApi.prototype.getHeroes = function(lang, cb) {
	var url = this.urlGetHeroes.format(this.apiKey, lang);
	request(url, function(e, response, body) {
		if(e) {
			cb(e);
		} else {
			if(response.statusCode == 200) {
				console.log(body);
				var result = bignumJSON.parse(body).result;
				console.log(result);
				if(result.heroes.length > 0) {
					cb(null, result.heroes);
				} else {
					cb({error: true, errorMsg: 'Can\'t find Heroes!'});
				}
			} else {
				cb({error: true, errorStatusCode: response.statusCode});
			}
		}
	});
};

WebApi.prototype.getPlayer = function(id, cb) {
	var id = long.fromInt(id);
	var base64 = long.fromString('76561197960265728');
	var id64 = base64.add(id);
	var url = this.urlGetPlayer.format(this.apiKey, id64);
	request(url, function(e, response, body) {
		if(e) {
			cb(e);
		} else {
			if(response.statusCode == 200) {
				var result = JSON.parse(body);
				cb(null, result.response.players);
			} else {
				cb({error: true, errorStatusCode: response.statusCode});
			}
		}
	});

};

WebApi.prototype.getTeamInfo = function(number, team, cb){
	var url = this.urlGetTeamInfo.format(this.apiKey, number, team);
	request(url, function(e, response, body){
		if(e) {
			cb(e);
		} else {
			if(response.statusCode == 200) {
				var result = bignumJSON.parse(body).result; 
				if(result.teams.length > 0){
					cb(null, result.teams);
				} else {
					cb({error: true, errorMsg: 'Team doesnt exist.'});
				}
			} else {
				cb({error: true, errorStatusCode: response.statusCode});
			}
		}
	});
};


WebApi.prototype.getUGCFile = function(ugcid, cb){
	var url = this.urlGetUGCFile.format(this.apiKey, ugcid);
	request(url, function(e, response, body){
		if(e) {
			cb(e);
		} else {
			if(response.statusCode == 200) {
				cb(null, bignumJSON.parse(body).data);
			} else {
				cb({error: true, errorStatusCode: response.statusCode});
			}
		}
	});
};

WebApi.prototype.getSchema = function(lang, cb){
	var url = this.urlGetSchema.format(this.apiKey, lang);
	request(url, function(e, response, body) {
		if(e) {
			cb(e);
		} else {
			if(response.statusCode == 200) {
				cb(null, bignumJSON.parse(body).result);
			} else {
				cb({error: true, errorStatusCode: response.statusCode});
			}
		}
	});
}

WebApi.prototype.downloadSchema = function(lang, name, cb){
	var url = this.urlGetSchema.format(this.apiKey, lang);
	httpgg.get(url, name, function (e, result) {
		if(e) {
			cb(e);
		} else {
			cb(null);
		}			
	});

};

WebApi.prototype.downloadItemsGame = function(name, cb){
	this.GetSchema('en', function(e, schema) {
		if(e) {
			cb(e);
		} else {
			var url = schema.items_game_url;
			httpgg.get(url, name, function(error, result){
				if(error) {
					cb(error);
				} else {
					cb(null);					
				}
			});
		}
	});
};

module.exports = WebApi;

if (!String.prototype.format) {
	String.prototype.format = function() {
		var args = arguments;
		return this.replace(/{(\d+)}/g, function(match, number) {
			return typeof args[number] != 'undefined'
				? args[number]
				: match
				;
		});
	};
}