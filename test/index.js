var webapi = require('./../index');

var WebApi = new webapi('');

// Should write proper tests.

/*

WebApi.getTournaments('en', function(e, tournaments){
	if(e) {
		console.log(e);
	} else {
		console.log(tournaments);
	}
});

*/



// Get Match Details
WebApi.getMatchDetails(1000443790, function(e, match) {
	if(e) {
		console.log(e);
	} else {
		console.log(match);
	}
});



/*

// Get Heroes list
WebApi.getHeroes('en', function(e, heroes) {
	if(e) {
		console.log(e);
	} else {
		console.log(heroes);
	}
});

*/

/*
// Get Player
WebApi.getPlayer('556111', function(e, player) {
	if(e) {
		console.log(e);
	} else {
		console.log(player);
	}
});
*/


/*

// Get Match History
WebApi.getMatchHistory('', '', '', '', '556111', '', '', 1, '', function(e, matches) {
	if(e) {
		console.log(e);
	} else {
		console.log(matches);
	}
});

*/

/*

// Get Match History by Sequence Num
WebApi.getMatchHistoryBySequenceNum(241, 1, function(e, matches) {
	if(e) {
		console.log(e);
	} else {
		console.log(matches);
	}
});

*/

/*
// Get Team Info
WebApi.getTeamInfo(5, function(e, info){
	if(e) {
		console.log(e);
	} else {
		console.log(info);
	}
});

*/


/*
// Get Team Logo
// Be carefull with big numbers
WebApi.getUGCFile('597017828284742302', function(e, data){
	if(e) {
		console.log(e);
	} else {
		console.log(data);
	}
});
*/
/*
// Download Schema
WebApi.downloadSchema('es', 'schema.js', function(e){
	if(e) {
		console.log(e);
	} else {
		console.log('File: Downloaded.');
	}
});

*/
/*
// Download Items Game
WebApi.downloadItemsGame('items_game.txt', function(e){
	if (e) {
		console.log(e);
	} else {
		console.log('Items Game Downloaded.');
	}
});
*/