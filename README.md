DotA 2 Web Api
=============
Easy-to-use API client for the Dota 2 WebAPI [http://wiki.teamfortress.com/wiki/WebAPI#Dota_2](http://wiki.teamfortress.com/wiki/WebAPI#Dota_2).
Heavily based on [https://www.npmjs.org/package/dota2-webapi](https://www.npmjs.org/package/dota2-webapi)

Installation
-----------
    npm install d2webapi

## How to use

The following example shows you how to use this library. In this case we are getting the league listing provided by the API.

```js
var webapi = require('d2webapi');

var WebApi = new webapi('APIKEY');

WebApi.getTournaments('en', function(e, tournaments){
	if(e){
		console.log(e);
	}else{
		console.log(tournaments);
	}
})
```

### Available methods

    getTournaments
    getMatchDetails
    getMatchHistory
    getMatchHistoryBySequenceNum
    getHeroes
    getTeamInfo
    getUGCFile